//
//  ViewController.swift
//  FinalProject
//
//  Copyright © 2017 Blacknight. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore

class ViewController: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()        
        let loginButton = LoginButton(readPermissions: [ .publicProfile ])
        //loginButton.center = view.center
        loginButton.frame = CGRect(x: 135, y: 500, width: 150, height: 40)
        view.addSubview(loginButton)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func Proceed(_ sender: UIButton) {
        
        if let accessToken = AccessToken.current {
            // User is logged in, use 'accessToken' here.
            self.performSegue(withIdentifier: "isLoggedIn", sender: self)
        }
        else {
            self.messageLabel.text = "Please, log in with your Facebook Account";
        }
    }
    
}

