//
//  MainPageController.swift
//  FinalProject
//
//  Copyright © 2017 Blacknight. All rights reserved.
//

import Foundation
import FacebookCore
import FacebookLogin

class MainPageController: UIViewController{
    @IBOutlet weak var welcomeMessage: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me")) { httpResponse, result in
            switch result {
            case .success(let response):
                let username = response.dictionaryValue!["name"] as! String
                self.welcomeMessage.text = self.welcomeMessage.text! + " " + username
                //print("Graph Request Succeeded: \(response)")
            case .failed(let error):
                print("Graph Request Failed: \(error)")
            }
        }
        connection.start()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
