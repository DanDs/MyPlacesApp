//
//  MapViewController.swift
//  FinalProject
//
//  Copyright © 2017 Blacknight. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import RealmSwift

class MapViewController: UIViewController {
    var realm:Realm?
    var places = [Place]()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func loadView() {
        
        let camera = GMSCameraPosition.camera(withLatitude: -17.372390,
                                              longitude: -66.162690,
                                              zoom: 16.5,
                                              bearing: 30,
                                              viewingAngle: 42)
        
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        do {
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("The style definition could not be loaded: \(error)")
        }
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        mapView.settings.indoorPicker = true
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures = false
        view = mapView
        
        // Creates a marker in the center of the map.
        /*
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -17.372390, longitude: -66.162690)
       
        let position2 = CLLocationCoordinate2D(latitude: -17.372409, longitude: -66.159776)
        */
        
        //Charge to BD
        realm = try! Realm()
     
        //retrieve from Realm
        let arrayPlaces = realm?.objects(MyPlacesDB.self)
        
        //iterate
        for placeinDB in arrayPlaces!{
            let newPlace = Place(id: placeinDB.id!, longitude: placeinDB.longitude, latitude: placeinDB.latitude,  title: placeinDB.title!, snippet: placeinDB.snippet!)
            places.append(newPlace!)
        }
        
        for placeToCharge in places{
            let mkr = GMSMarker()
            mkr.position = CLLocationCoordinate2D(latitude: placeToCharge.latitude!, longitude: placeToCharge.longitude!)
            mkr.title = placeToCharge.title!
            mkr.snippet = placeToCharge.snippet!
            mkr.map = mapView
        }
    }
}
