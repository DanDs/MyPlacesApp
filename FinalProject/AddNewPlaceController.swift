//
//  AddNewPlaceController.swift
//  FinalProject
//
//  Copyright © 2017 Blacknight. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class AddNewPlaceController: UIViewController {
    @IBOutlet weak var longitudeField: UITextField!
    @IBOutlet weak var latitudeField: UITextField!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var snippetField: UITextField!
    @IBOutlet weak var longitudeErrorMessage: UILabel!
    @IBOutlet weak var latitudeErrorMessage: UILabel!
    @IBOutlet weak var titleErrorMessage: UILabel!
    @IBOutlet weak var snippetErrorMessage: UILabel!
    var realm:Realm?
    override func viewDidLoad() {
        super.viewDidLoad()
         realm = try! Realm()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func savePlace(place: Place){
        let newPlace = MyPlacesDB()
        
        newPlace.id = place.id
        newPlace.latitude = (place.latitude)!
        newPlace.longitude = (place.longitude)!
        newPlace.title = place.title
        newPlace.snippet = place.snippet
        
        print ("the uuid --> \(UIDevice.current.identifierForVendor)")
        try! self.realm?.write {
            
            print("Save !!! --Paht --> \( Realm.Configuration.defaultConfiguration.fileURL)")
            self.realm?.add(newPlace)
        }
        print("Guardado!")
    
        self.performSegue(withIdentifier: "isSaved", sender: self)
        
    }
    
    
    @IBAction func Save(_ sender: UIButton) {
       
        if(titleField.text!.isEmpty || longitudeField.text!.isEmpty || latitudeField.text!.isEmpty || snippetField.text!.isEmpty){
            if(longitudeField.text!.isEmpty){
                longitudeErrorMessage.text = "Please enter the longitude"
            }
            if(latitudeField.text!.isEmpty){
                latitudeErrorMessage.text = "Please enter the latitude"
            }
            if(titleField.text!.isEmpty){
                titleErrorMessage.text = "Please enter a title"
            }
            if(snippetField.text!.isEmpty){
                snippetErrorMessage.text = "Please add a description"
            }            
        }
        else{
            var place1 = Place(id: NSUUID().uuidString, longitude: Double(longitudeField.text!)!, latitude: Double(latitudeField.text!)!, title: titleField.text!, snippet: snippetField.text!)
            savePlace(place: place1!)
        
        }
    }
    
}

