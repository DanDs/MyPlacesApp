//
//  MyPlacesDB.swift
//  FinalProject
//
//  Copyright © 2017 Blacknight. All rights reserved.
//

import Foundation
import RealmSwift
class MyPlacesDB:Object{
    @objc dynamic var id:String?
    @objc dynamic var longitude:Double = 0.0
    @objc dynamic var latitude:Double = 0.0
    @objc dynamic var title:String?
    @objc dynamic var snippet:String?
    override static func primaryKey() -> String?{
        return "id"
    }
}

