//
//  Place.swift
//  FinalProject
//
//  Copyright © 2017 Blacknight. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class Place {
    var id:String?
    var longitude:Double?
    var latitude:Double?
    var title:String?
    var snippet:String?
    
    
    init?(id:String, longitude:Double, latitude:Double, title:String, snippet:String){
        self.id = id
        self.longitude = longitude
        self.latitude = latitude
        self.title = title
        self.snippet = snippet
        if(longitude.isNaN || latitude.isNaN){
            return nil
        }
    }
}
