//
//  PlacesTableViewController.swift
//  FinalProject
//
//  Created by Dan on 12/11/17.
//  Copyright © 2017 Blacknight. All rights reserved.
//


import UIKit
import RealmSwift
class PlacesTableViewController: UITableViewController {
    var markers = [Place]()
    var realm:Realm?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //connect to DB
        realm = try! Realm()
    
        //retrieve from Realm
        let arrayMeals = realm?.objects(MyPlacesDB.self)
        
        //iterate
        for placeinDB in arrayMeals!{
            var newPlace = Place(id: placeinDB.id!, longitude: placeinDB.longitude,  latitude: placeinDB.latitude, title: placeinDB.title!, snippet: placeinDB.snippet!)
            markers.append(newPlace!)
        }
        
        navigationItem.leftBarButtonItem = editButtonItem
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return markers.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! PlacesTableViewCell
        // get Model meal
        let place = markers[indexPath.row]
        cell.title.text = place.title
        cell.snippet.text = place.snippet
        cell.longitude.text = String(place.longitude!)
        cell.latitude.text = String(place.latitude!) //.description
        
        // Configure the cell...
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            markers.remove(at: indexPath.row)
            
            let placeToRemove = markers[indexPath.row]
            let dbObjbToRemove = realm?.objects(MyPlacesDB.self).filter("id == %@", placeToRemove.id).first
            
            //delete Meal
            try! realm?.write {
                realm?.delete(dbObjbToRemove!)
                //meals.remove(at: indexPath.row)
            }
            
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
 
    
   
    
    
}

