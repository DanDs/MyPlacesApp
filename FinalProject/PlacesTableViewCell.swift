//
//  PlacesTableViewCell.swift
//  FinalProject
//
//  Created by Dan on 12/11/17.
//  Copyright © 2017 Blacknight. All rights reserved.
//

import UIKit

class PlacesTableViewCell: UITableViewCell{
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var snippet: UILabel!
    @IBOutlet weak var longitude: UILabel!
    @IBOutlet weak var latitude: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
    }
}
